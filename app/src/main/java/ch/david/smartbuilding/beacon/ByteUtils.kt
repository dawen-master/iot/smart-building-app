package ch.david.smartbuilding.beacon

/**
 * Takes the value of a signed byte, considers it as unsigned (positive) and converts it into a signed Int.
 * This function is useful only while unsigned bytes aren't supported in JVM Kotlin (version < 1.3).
 * @return Positive signed Int
 */
fun Byte.toPositiveInt() = toInt() and 0xFF
// Source: https://stackoverflow.com/a/38660733

/**
 * Takes the value of a signed byte, considers it as unsigned (positive) and converts it into a signed Long.
 * This function is useful only while unsigned bytes aren't supported in JVM Kotlin (version < 1.3).
 * @return Positive signed Long
 */
fun Byte.toPositiveLong() = toLong() and 0xFF
// Source: https://stackoverflow.com/a/38660733

class ByteUtils {

    companion object {

        /**
         * Converts a ByteArray into a string with hexadecimal format.
         * @param array ByteArray to convert
         * @return String representing the byte array, in hexadecimal format.
         */
        fun bytesToString(array: ByteArray) : String {
            val sb = StringBuilder()

            for (octet in array) {
                sb.append(String.format("%02x", octet))
            }

            return sb.toString()
        }

        /**
         * Converts a list of bytes into a string with hexadecimal format.
         * @param array List of bytes to convert
         * @return String representing the byte array, in hexadecimal format.
         */
        fun bytesToString(array: List<Byte>) : String {
            val sb = StringBuilder()

            for (octet in array) {
                sb.append(String.format("%02x", octet))
            }

            return sb.toString()
        }

        /**
         * Converts a list of bytes into a signed Int.
         * @param octets List of bytes
         * @return Signed Int
         */
        fun bytesToInt(octets: List<Byte>) : Int {
            var value: Int = 0

            for (octet in octets) {
                // Shift left of one byte length, that is 8 bits.
                value = (value shl 8)
                val lsByte = octet.toPositiveInt()
                // Put the low byte at the end of the shifted value.
                value = value or lsByte
            }

            return value
        }

        /**
         * Converts a list of bytes into a signed Long.
         * @param octets List of bytes
         * @return Signed Long
         */
        fun bytesToLong(octets: List<Byte>) : Long {
            var value: Long = 0

            for (octet in octets) {
                // Shift left of one byte length, that is 8 bits.
                value = (value shl 8)
                val lsByte = octet.toPositiveLong()
                // Put the low byte at the end of the shifted value.
                value = value or lsByte
            }

            return value
        }
    }
}