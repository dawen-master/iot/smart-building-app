package ch.david.smartbuilding.beacon

import java.nio.ByteBuffer
import java.util.UUID

/**
 * Represents an iBeacon Advertisement Structure
 */
class IBeaconAdStructure(organizationUuid: UUID, major: Int, minor: Int, txPower: Int)
{
    /**
     * Organization UUID of the advertising beacon.
     */
    val organizationUuid = organizationUuid
    /**
     * Major number of the advertising beacon.
     */
    val major = major
    /**
     * Minor number of the advertising beacon.
     */
    val minor = minor
    /**
     * Transmission power in dBm at 1 meter from the beacon.
     */
    val txPower = txPower

    companion object
    {
        /**
         * Length of an iBeacon payload: 25 bytes.
         * It is counted without the byte indicating the advertising
         * data type, which is "Manufacturer Specific Data" (0xFF) for iBeacons.
         */
        const val IBEACON_PAYLOAD_LENGTH : Int = 25

        /**
         * Tries to extract an iBeacon advertising structure from a list of bytes representing
         * a Bluetooth Low Energy advertising structure.
         * More details about iBeacon advertising data:
         * https://support.kontakt.io/hc/articles/201492492-iBeacon-advertising-packet-structure
         * @param payload Bytes of the content of an iBeacon advertising structure,
         * without the header byte (length-indication byte).
         * @return An iBeacon advertisement structure if success, otherwise null.
         */
        fun build(payload : List<Byte>) : IBeaconAdStructure?
        {
            // Check if the length corresponds to the length of an iBeacon advertising structure.
            if (payload.size == IBEACON_PAYLOAD_LENGTH)
            {
                //println(ByteUtils.bytesToString(payload))

                val manufacturerBytes = payload.slice(IntRange(0, 3))

                // TODO
                // Filter manufacturer info and return early if it doesn't correspond to an iBeacon packet.

                val uuidBytes = payload.slice(IntRange(4, 19))
                val major = ByteUtils.bytesToInt(payload.slice(IntRange(20, 21)))
                val minor = ByteUtils.bytesToInt(payload.slice(IntRange(22, 23)))
                val txPower = ByteUtils.bytesToInt(payload.slice(IntRange(24, 24)))

                // 8 most significant bytes of the UUID (high part)
                val hiUuid = ByteUtils.bytesToLong(uuidBytes.slice(IntRange(0, 7)))

                // 8 least significant bytes of the UUID (low part)
                val loUuid = ByteUtils.bytesToLong(uuidBytes.slice(IntRange(8, 15)))

                val uuid: UUID = UUID(hiUuid, loUuid)

                return IBeaconAdStructure(uuid, major, minor, txPower)
            }

            return null
        }


    }
}