package ch.david.smartbuilding.beacon

import java.util.*

/**
 * Represents a beacon device.
 */
class Beacon(organizationUuid: UUID, major: Int, minor: Int)
{
    /**
     * Organization UUID of the advertising beacon.
     */
    val organizationUuid = organizationUuid
    /**
     * Major number of the advertising beacon.
     */
    val major = major
    /**
     * Minor number of the advertising beacon.
     */
    val minor = minor

    /**
     * Ratio of transmission power over RSSI (unitless).
     */
    var powerRatio: Double = Double.MIN_VALUE

    /**
     * Returns true if both beacons have the same
     * organization UUID, major and minor numbers.
     */
    fun isEquivalent(beacon: Beacon) : Boolean
    {
        return major == beacon.major &&
                minor == beacon.minor &&
                organizationUuid == beacon.organizationUuid
    }

}