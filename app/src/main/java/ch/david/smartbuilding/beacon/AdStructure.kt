package ch.david.smartbuilding.beacon

/**
 * Represents a Bluetooth Low Energy advertising structure.
 */
class AdStructure(val dataType: ADType, val payload: List<Byte>)
{
    //         BLE Advertising Structure
    //
    // |<---1B--->|<---1B--->|<---Length - 1-->|
    // +---------------------------------------+
    // |  Length  | AD Type  |     Payload     |
    // +---------------------------------------+
    //
    // All possible AD types (advertising data types) are listed at:
    // https://www.bluetooth.com/specifications/assigned-numbers/generic-access-profile
    //

    companion object
    {
        /**
         * Builds an advertisement structure from a list of bytes coming from a
         * Bluetooth Low Energy advertisement packet (scan packet).
         * @param rawData List of byte extracted from ScanResult.scanRecord.bytes,
         * without the "Length" byte but with the "AD Type" byte.
         * @return AdStructure built from the provided bytes list.
         */
        fun build(rawData: List<Byte>) : AdStructure?
        {
            //The advertisement structure must contain at least 1 byte, the "AD Type" byte.
            if (rawData.isNotEmpty())
            {
                val adType: ADType? = ADType.fromByte(rawData[0])
                if (adType != null)
                {
                    //Get the payload, if any.
                    val payload = if (rawData.size > 1)
                        rawData.slice(IntRange(1, rawData.size - 1)) else emptyList()

                    return AdStructure(adType, payload)
                }
            }

            return null
        }

        /**
         * Builds a list of advertisement structures from Advertising Data bytes.
         * The Advertising Data bytes come from ScanResult.scanRecord.bytes.
         * More details about advertising data and structures:
         * https://docs.mbed.com/docs/ble-intros/en/master/Advanced/CustomGAP/
         * @param advData Advertising Data bytes
         * @return List of advertisement structures that have been found
         * from the Advertising Data bytes.
         */
        fun extractAdStructures(advData: ByteArray) : List<AdStructure>
        {
            //+-------------------------------------+
            //|          Advertising Data           |
            //+-------------------------------------+
            //| Ad structure  | Ad structure  | ... |
            //+-------------------------------------+
            //| Length | Data | Length | Data | ... |
            //+-------------------------------------+

            val adStructures : MutableList<AdStructure> = mutableListOf()

            var i : Int = 0
            while (i < advData.size)
            {
                // Number of bytes declared in the advertising structure, without counting the current byte.
                val structureSize = advData[i].toPositiveInt()

                if (structureSize == 0)
                // Reached end of packet (padded with 0 at the end).
                    break

                // Number of bytes remaining in the array, without counting the current byte.
                val remainingSize = advData.size - (i + 1)

                // The number of bytes remaining in the advertising data must be greater or equal to
                // the declared size of the advertising structure, otherwise the frame is malformed.
                if (remainingSize < structureSize)
                    break

                // Byte array of the advertisement structure.
                val rawAdStructure = advData.slice(IntRange(i + 1, i + structureSize))

                // Build an Advertisement structure object from the byte array.
                val adStructure = AdStructure.build(rawAdStructure)

                if (adStructure != null)
                    adStructures.add(adStructure)

                // Go to the index of the next advertisement structure.
                i += structureSize + 1
            }

            return adStructures
        }

    }
}