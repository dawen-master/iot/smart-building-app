package ch.david.smartbuilding.beacon

import android.bluetooth.le.ScanResult
import android.util.Log
import java.util.*

class BeaconDetector()
{
    /**
     * Organization UUID of all beacons of the IoT course.
     */
    val COURSE_UUID: UUID = UUID.fromString("b9407f30-f5f8-466e-aff9-25556b57fe6d")
    /**
     * Major number of the beacons of our group.
     */
    val GROUP_MAJOR = 21745 // = 0x54F1

    /**
     * Minor number of the blue beacon.
     */
    val BLUE_BEACON_MAJOR = 32753 // = 0x7FF1
    /**
     * Minor number of the pink beacon.
     */
    val PINK_BEACON_MINOR = 57473 // = 0xE081

    val blueBeacon: Beacon = Beacon(COURSE_UUID, major = GROUP_MAJOR, minor = BLUE_BEACON_MAJOR)
    val pinkBeacon: Beacon = Beacon(COURSE_UUID, major = GROUP_MAJOR, minor = PINK_BEACON_MINOR)

    val BLUE_BEACON_ROOM_NUMBER: Int = 1
    val PINK_BEACON_ROOM_NUMBER: Int = 2

    var closestBeacon: Beacon? = null

    /**
     * Finds the number of the closest room from a Bluetooth advertisement packet.
     * For the BLUE beacon, returns 1;
     * for the PINK beacon, returns 2;
     * for unknown other advertising devices, returns 0.
     */
    fun findRoomNumber(scanResult: ScanResult) : Int
    {
        val iBeaconAdStructure : IBeaconAdStructure? = extractBeaconAdStructure(scanResult)
        if (iBeaconAdStructure != null)
        {
            val beacon: Beacon? = findBeacon(iBeaconAdStructure)
            if (beacon != null)
            {
                val powerRatio: Double = scanResult.rssi.toDouble() / iBeaconAdStructure.txPower
                beacon.powerRatio = powerRatio

                if (blueBeacon.powerRatio > pinkBeacon.powerRatio)
                {
                    closestBeacon = blueBeacon
                    Log.d(this.javaClass.simpleName, "[PROX] BLUE beacon is closer.")
                    return BLUE_BEACON_ROOM_NUMBER
                }
                else
                {
                    closestBeacon = pinkBeacon
                    Log.d(this.javaClass.simpleName, "[PROX] PINK beacon is closer.")
                    return PINK_BEACON_ROOM_NUMBER
                }
            }
        }

        return 0
    }

    fun findBeacon(iBeaconAdStructure: IBeaconAdStructure) : Beacon?
    {
        if (isOwnBeacon(iBeaconAdStructure))
        {
            val beacon: Beacon? = when (iBeaconAdStructure.minor)
            {
                blueBeacon.minor -> blueBeacon
                pinkBeacon.minor -> pinkBeacon
                else -> null
            }

            return beacon
        }

        return null
    }

    fun extractBeaconAdStructure(scanResult: ScanResult) : IBeaconAdStructure?
    {
        val scanRecord = scanResult.scanRecord
        if (scanRecord != null)
        {
            val address = scanResult.device?.address

            val advData = scanRecord.bytes
            if (advData != null)
            {
                val adStructures: List<AdStructure> = AdStructure.extractAdStructures(advData)

                //Try to find iBeacon advertisement structure in the packet.
                val manufacturerAdStructure = adStructures.firstOrNull { it -> it.dataType == ADType.Manufacturer_Specific_Data };
                if (manufacturerAdStructure != null)
                {
                    val iBeaconAdStructure = IBeaconAdStructure.build(manufacturerAdStructure.payload)

                    return iBeaconAdStructure
                }
            }

            //Estimote fe9a
            //Estimote 0000fe9a-0000-1000-8000-00805f9b34fb
        }

        return null
    }

    fun isOwnBeacon (iBeaconAdStructure: IBeaconAdStructure) : Boolean
    {
        return COURSE_UUID.equals(iBeaconAdStructure.organizationUuid) &&
                GROUP_MAJOR == iBeaconAdStructure.major;
    }
}