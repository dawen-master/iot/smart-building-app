package ch.david.smartbuilding.beacon

/**
 * Byte enum representing the Advertising Data Type (AD Type).
 * Complete list at:
 * https://www.bluetooth.com/specifications/assigned-numbers/generic-access-profile
 */
enum class ADType(val value : Byte)
{
    Flags(0x01),
    Incomplete_List_of_16bit_Service_Class_UUIDs(0x02),
    Complete_List_of_16bit_Service_Class_UUIDs(0x03),
    Incomplete_List_of_32bit_Service_Class_UUIDs(0x04),
    Complete_List_of_32bit_Service_Class_UUIDs(0x05),
    Incomplete_List_of_128bit_Service_Class_UUIDs(0x06),
    Complete_List_of_128bit_Service_Class_UUIDs(0x07),
    Shortened_Local_Name(0x08),
    Complete_Local_Name(0x09),
    Tx_Power_Level(0x0A),
    Class_of_Device(0x0D),
    Simple_Pairing_Hash_C(0x0E),
    Simple_Pairing_Hash_C192(0x0E),
    Simple_Pairing_Randomizer_R(0x0F),
    Simple_Pairing_Randomizer_R192(0x0F),
    Device_ID(0x10),
    Security_Manager_TK_Value(0x10),
    Security_Manager_Out_of_Band_Flags(0x11),
    Slave_Connection_Interval_Range(0x12),
    List_of_16bit_Service_Solicitation_UUIDs(0x14),
    List_of_128bit_Service_Solicitation_UUIDs(0x15),
    Service_Data(0x16),
    Service_Data_16bit_UUID(0x16),
    Public_Target_Address(0x17),
    Random_Target_Address(0x18),
    Appearance(0x19),
    Advertising_Interval(0x1A),
    LE_Bluetooth_Device_Address(0x1B),
    LE_Role(0x1C),
    Simple_Pairing_Hash_C256(0x1D),
    Simple_Pairing_Randomizer_R256(0x1E),
    List_of_32bit_Service_Solicitation_UUIDs(0x1F),
    Service_Data_32bit_UUID(0x20),
    Service_Data_128bit_UUID(0x21),
    LE_Secure_Connections_Confirmation_Value(0x22),
    LE_Secure_Connections_Random_Value(0x23),
    URI(0x24),
    Indoor_Positioning(0x25),
    Transport_Discovery_Data(0x26),
    LE_Supported_Features(0x27),
    Channel_Map_Update_Indication(0x28),
    PB_ADV(0x29),
    Mesh_Message(0x2A),
    Mesh_Beacon(0x2B),
    Information_Data_3D(0x3D),
    Manufacturer_Specific_Data(0xFF.toByte());

    companion object {
        private val map= ADType.values().associateBy(ADType::value);

        /**
         * Gets an enum object that corresponds to a given byte value.
         * @param[octet] The value of the enum object to retrieve.
         * @return ADType object that corresponds to the byte value, otherwise null.
         */
        fun fromByte(octet: Byte): ADType?
        {
            return map.get(octet)

            //Source:
            // https://stackoverflow.com/a/37795810
            // "Effective Enums in Kotlin with reverse lookup?"
        }
    }
}
