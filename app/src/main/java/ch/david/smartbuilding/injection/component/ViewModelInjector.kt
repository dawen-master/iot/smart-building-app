package ch.david.smartbuilding.injection.component

import ch.david.smartbuilding.injection.module.NetworkModule
import ch.david.smartbuilding.ui.main.MainViewModel
import dagger.Component
import javax.inject.Singleton

/**
 * @author David Wittwer & Joao Mendes
 *
 * @version 0.1
 *
 * Created on 26/10/18 by David Wittwer
 */
@Singleton
@Component(modules = [(NetworkModule::class)])
interface ViewModelInjector {

    fun inject(mainViewModel: MainViewModel)

    @Component.Builder
    interface Builder {
        fun build(): ViewModelInjector

        fun networkModule(networkModule: NetworkModule): Builder
    }
}