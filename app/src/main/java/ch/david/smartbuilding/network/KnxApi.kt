package ch.david.smartbuilding.network

import ch.david.smartbuilding.model.FacilityData
import retrofit2.Call
import retrofit2.http.*

/**
 * @author David Wittwer & Joao Mendes
 *
 * @version 0.1
 *
 * Created on 25.10.18 by David Wittwer
 */
interface KnxApi {
    @GET("/knx/{room_key}/{facility_key}/value")
    fun getFacilityValue(@Path("room_key") roomKey: String, @Path("facility_key") facilityKey: String): Call<Int>

    @Headers("Content-Type: application/json")
    @POST("/knx/{room_key}/{facility_key}/value")
    fun setFacilityValue(@Path("room_key") roomKey: String, @Path("facility_key") facilityKey: String, @Body data: FacilityData): Call<Int>
}