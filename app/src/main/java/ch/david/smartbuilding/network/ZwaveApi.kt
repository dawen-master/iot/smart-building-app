package ch.david.smartbuilding.network

import ch.david.smartbuilding.model.Dimmer
import ch.david.smartbuilding.model.DimmerLevel
import ch.david.smartbuilding.model.Network
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

/**
 * @author David Wittwer & Joao Mendes
 *
 * @version 0.1
 *
 * Created on 25.10.18 by David Wittwer
 */
public interface ZwaveApi {
    @GET("/network/info")
    fun getNetworkInfo(): Call<Network>

    @GET("/dimmers/{id}/get_level")
    fun getDimmerLevel(@Path("id") id: Int): Call<Dimmer>

    @POST("/dimmers/set_level")
    fun setDimmerLevel(@Body level: DimmerLevel): Call<String>
}