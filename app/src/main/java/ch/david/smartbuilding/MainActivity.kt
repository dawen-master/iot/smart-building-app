package ch.david.smartbuilding

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import ch.david.smartbuilding.ui.main.MainFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

    }

}
