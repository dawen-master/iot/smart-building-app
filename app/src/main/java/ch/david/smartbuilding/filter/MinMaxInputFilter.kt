package ch.david.smartbuilding.filter

import android.text.InputFilter
import android.text.Spanned


/**
 * @author David Wittwer & Joao Mendes
 *
 * @version 0.1
 *
 * Created on 20.10.18 by David Wittwer
 */
class MinMaxInputFilter(private val min: Int, private val max: Int) : InputFilter {
    constructor(min: String, max: String) : this(min.toInt(), max.toInt())

    override fun filter(
        source: CharSequence,
        start: Int,
        end: Int,
        dest: Spanned,
        dstart: Int,
        dend: Int
    ): CharSequence? {
        try {
            val input = Integer.parseInt(dest.toString() + source.toString())
            if (isInRange(min, max, input))
                return null
        } catch (nfe: NumberFormatException) {
        }

        return ""
    }

    private fun isInRange(a: Int, b: Int, c: Int): Boolean {
        return if (b > a) c in a..b else c in b..a
    }

}