package ch.david.smartbuilding.ui.main

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.bluetooth.BluetoothManager
import android.bluetooth.le.BluetoothLeScanner
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.content.Context
import android.os.Handler
import android.util.Log
import ch.david.smartbuilding.base.BaseViewModel
import ch.david.smartbuilding.beacon.BeaconDetector
import ch.david.smartbuilding.model.DimmerLevel
import ch.david.smartbuilding.model.FacilityData
import ch.david.smartbuilding.network.KnxApi
import ch.david.smartbuilding.network.ZwaveApi
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg
import javax.inject.Inject

class MainViewModel : BaseViewModel() {
    private lateinit var knxApi: KnxApi
    private lateinit var zwaveApi: ZwaveApi

    private val percent = MutableLiveData<Int>()
    private val lightPercent = MutableLiveData<Int>()
    private val blindPercent = MutableLiveData<Int>()
    private val valvePercent = MutableLiveData<Int>()
    private var count = DEFAULT
    private var currentSync: Ctrl? = null

    private val roomNumber = MutableLiveData<Int>()

    private val lightHandler = Handler()
    private val blindHandler = Handler()
    private val valveHandler = Handler()

    val beaconDetector: BeaconDetector = BeaconDetector()

    init {
        percent.value = count
        lightPercent.value = DEFAULT
        blindPercent.value = DEFAULT
        valvePercent.value = DEFAULT
        roomNumber.value = 1

        loadValues()
    }

    @Inject
    fun setKnxApi(knxApi: KnxApi) {
        this.knxApi = knxApi
    }

    @Inject
    fun setZwaveApi(zwaveApi: ZwaveApi) {
        this.zwaveApi = zwaveApi
    }


    fun getRoomNumber(): LiveData<Int> = roomNumber

    fun getPercent(): LiveData<Int> = percent

    fun getLightPercent(): LiveData<Int> = lightPercent
    fun getBlindPercent(): LiveData<Int> = blindPercent
    fun getValvePercent(): LiveData<Int> = valvePercent

    fun syncWith(ctrl: Ctrl?) {
        currentSync = ctrl
        percent.value = when (ctrl) {
            MainViewModel.Ctrl.LIGHT -> lightPercent.value
            MainViewModel.Ctrl.BLIND -> blindPercent.value
            MainViewModel.Ctrl.VALVE -> valvePercent.value
            null -> DEFAULT
        }
        count = percent.value!!

        loadValues()
    }

    fun setToMax() {
        count = MAX
        percent.value = MAX
        sync()
    }

    fun setToMin() {
        count = MIN
        percent.value = MIN
        sync()
    }

    fun incrementPercent() {
        if (MAX > percent.value!!) {
            percent.value = ++count
            sync()
        }
    }

    fun decrementPercent() {
        if (MIN < percent.value!!) {
            percent.value = --count
            sync()
        }
    }

    fun setToMiddle() {
        percent.value = (MAX - MIN) / 2
        sync()
    }

    private fun sync() {
        when (currentSync) {
            Ctrl.LIGHT -> {
                lightHandler.removeCallbacksAndMessages(null)
                lightHandler.postDelayed(syncLight(), UPDATE_DELAY)
            }
            Ctrl.BLIND -> {
                blindHandler.removeCallbacksAndMessages(null)
                blindHandler.postDelayed(syncBlind(), UPDATE_DELAY)
            }
            Ctrl.VALVE -> {
                valveHandler.removeCallbacksAndMessages(null)
                valveHandler.postDelayed(syncValve(), UPDATE_DELAY)
            }
        }
    }

    private fun getRoom(): String {
        return "room" + roomNumber.value
    }

    private fun syncLight() = Runnable {
        if (null === getLightNodeId())
            return@Runnable
        async(UI) {
            val bgJob = bg {
                zwaveApi.setDimmerLevel(
                    DimmerLevel(
                        getLightNodeId()!!.toString(),
                        percent.value!!.toString()
                    )
                )
            }
            bgJob.await()
            blindPercent.value = percent.value
        }
    }

    private fun syncBlind() = Runnable {
        async(UI) {
            val fValue = percent.value!!.toFloat() / MAX * MAX_VALUE
            val bgJob = bg {
                knxApi.setFacilityValue(getRoom(), "blind1", FacilityData(fValue.toInt()))
                    .execute()
                knxApi.setFacilityValue(getRoom(), "blind2", FacilityData(fValue.toInt()))
                    .execute()
            }
            bgJob.await()
            blindPercent.value = percent.value
        }
    }

    private fun syncValve() = Runnable {
        async(UI) {
            val fValue = percent.value!!.toFloat() / 100 * MAX_VALUE
            val bgJob = bg {
                knxApi.setFacilityValue(getRoom(), "valve1", FacilityData(fValue.toInt()))
                    .execute()
                knxApi.setFacilityValue(getRoom(), "valve2", FacilityData(fValue.toInt()))
                    .execute()
            }
            bgJob.await()
            valvePercent.value = percent.value
        }
    }

    private fun loadValues() {
        bg {
            val nodeId = getLightNodeId()
            if (null !== nodeId) {
                val lightResponse = zwaveApi.getDimmerLevel(nodeId).execute()
                if (lightResponse.isSuccessful)
                    blindPercent.value = lightResponse.body()!!.value
                else
                    blindPercent.value = -1
            }

            val blindResponse = knxApi.getFacilityValue(getRoom(), "blind1").execute()
            if (blindResponse.isSuccessful) {
                val v = blindResponse.body()!!.toFloat()
                blindPercent.value = (v / MAX_VALUE * 100).toInt()
            } else
                blindPercent.value = -1


            val valveResponse = knxApi.getFacilityValue(getRoom(), "valve1").execute()
            if (valveResponse.isSuccessful) {
                val v = valveResponse.body()!!.toFloat()
                valvePercent.value = (v / MAX_VALUE * 100).toInt()
            } else
                valvePercent.value = -1
        }
    }

    private fun getLightNodeId(): Int? {
//        TODO use dynamic mapping between room number and node id
        return when (roomNumber.value) {
            1 -> 2
            2 -> 3
            else -> null
        }
    }


    private fun getBleScanner(appContext: Context): BluetoothLeScanner? {
        val bluetoothManager =
            appContext.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        val bluetoothAdapter = bluetoothManager.adapter
        return bluetoothAdapter?.bluetoothLeScanner
    }

    fun startScan(appContext: Context) {
        getBleScanner(appContext)?.startScan(bleScanCallback)
    }

    companion object {
        private const val UPDATE_DELAY = 2000L
        private const val DEFAULT = 50
        private const val MAX = 100
        private const val MIN = 0
        private const val MAX_VALUE = 255
        private const val MIN_VALUE = 0
    }

    enum class Ctrl { LIGHT, BLIND, VALVE }

    private val bleScanCallback = object : ScanCallback() {
        private val TAG = "BleScanCallback"

        override fun onScanResult(callbackType: Int, scanResult: ScanResult?) {
            if (scanResult != null) {
                val number = beaconDetector.findRoomNumber(scanResult)
                if (number != 0) {
                    Log.d(TAG, "[PROX] Room $number is closer.")
                    roomNumber.postValue(number)
                }
            }
        }
    }
}
