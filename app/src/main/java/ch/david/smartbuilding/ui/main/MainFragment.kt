package ch.david.smartbuilding.ui.main

import android.Manifest
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.content.PermissionChecker
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ToggleButton
import ch.david.smartbuilding.R
import kotlinx.android.synthetic.main.main_fragment.*


class MainFragment : Fragment() {

    companion object {
        private const val REP_DELAY = 100L
        private const val REP_DELAY_FAST = 70L
        private const val ACC = 2000
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel

    private val repeatHandler = Handler()
    private var time = 0L
    private var mAutoIncrement = false
    private var mAutoDecrement = false
    private var selectedBtn: ButtonsTypes? = null

    /**
     * {@inheritDoc}
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)

        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    /**
     * {@inheritDoc}
     */
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)

        tryStartScan()

//        Observe view model data
        viewModel.getRoomNumber().observe(this, Observer { roomNumber ->
            tvRoom.text = getString(R.string.currentRoom, roomNumber)
        })

        viewModel.getPercent().observe(this, Observer {
            if (null == it || -1 == it)
                etPercent.text = getString(R.string.labelUnknown)
            else
                etPercent.text = it.toString(10)
        })

        viewModel.getLightPercent().observe(this, Observer {
            if (null == it || -1 == it)
                tvLight.text = getString(R.string.labelUnknown)
            else
                tvLight.text = it.toString(10)
        })

        viewModel.getBlindPercent().observe(this, Observer {
            if (null == it || -1 == it)
                tvBlind.text = getString(R.string.labelUnknown)
            else
                tvBlind.text = it.toString(10)
        })

        viewModel.getValvePercent().observe(this, Observer {
            if (null == it || -1 == it)
                tvRadiator.text = getString(R.string.labelUnknown)
            else
                tvRadiator.text = it.toString(10)
        })

//        Increment on click
        btnInc.setOnClickListener {
            viewModel.incrementPercent()
        }

//        Rapid increment on long click
        btnInc.setOnLongClickListener {
            mAutoIncrement = true
            time = 0
            repeatHandler.post(RptUpdater())
            true
        }

        btnInc.setOnTouchListener { _, event ->
            if ((event.action == MotionEvent.ACTION_UP || event.action == MotionEvent.ACTION_CANCEL)
                && mAutoIncrement
            ) {
                mAutoIncrement = false
            }
            false
        }

//        Decrement on click
        btnDec.setOnClickListener {
            viewModel.decrementPercent()
        }

//        Rapid decrement on long click
        btnDec.setOnLongClickListener {
            mAutoDecrement = true
            time = 0
            repeatHandler.post(RptUpdater())
            true
        }

        btnDec.setOnTouchListener { _, event ->
            if ((event.action == MotionEvent.ACTION_UP || event.action == MotionEvent.ACTION_CANCEL)
                && mAutoDecrement
            ) {
                mAutoDecrement = false
            }
            false
        }

//        Set full up and down btn
        btnUpFull.setOnClickListener { viewModel.setToMax() }
        btnDownFull.setOnClickListener { viewModel.setToMin() }
        btnMiddle.setOnClickListener { viewModel.setToMiddle() }

//        Setup action btn
        btnLight.setOnClickListener {
            if (ButtonsTypes.LIGHT !== selectedBtn)
                deselectBtn()

            selectedBtn = if (ButtonsTypes.LIGHT == selectedBtn) {
                viewModel.syncWith(null)
                null
            } else {
                viewModel.syncWith(MainViewModel.Ctrl.LIGHT)
                ButtonsTypes.LIGHT
            }
        }

        btnBlind.setOnClickListener {
            if (ButtonsTypes.BLIND !== selectedBtn)
                deselectBtn()

            selectedBtn = if (ButtonsTypes.BLIND == selectedBtn) {
                viewModel.syncWith(null)
                null
            } else {
                viewModel.syncWith(MainViewModel.Ctrl.BLIND)
                ButtonsTypes.BLIND
            }
        }

        btnRadiator.setOnClickListener {
            if (ButtonsTypes.RADIATOR !== selectedBtn)
                deselectBtn()

            selectedBtn = if (ButtonsTypes.RADIATOR == selectedBtn) {
                viewModel.syncWith(null)
                null
            } else {
                viewModel.syncWith(MainViewModel.Ctrl.VALVE)
                ButtonsTypes.RADIATOR
            }
        }
    }

    /**
     * Deselect current selected button
     */
    private fun deselectBtn() {
        Log.d("IOT", "Deselect btn $selectedBtn")
        when (selectedBtn) {
            ButtonsTypes.LIGHT -> (btnLight as ToggleButton).isChecked = false
            ButtonsTypes.BLIND -> (btnBlind as ToggleButton).isChecked = false
            ButtonsTypes.RADIATOR -> (btnRadiator as ToggleButton).isChecked = false
        }
    }

    /**
     * Runnable class for fast increment on long click
     */
    inner class RptUpdater : Runnable {

        override fun run() {
            Log.d("IOT", time.toString(10))
            val delay = if (ACC > time) {
                REP_DELAY
            } else {
                REP_DELAY_FAST
            }

            if (mAutoIncrement) {
                viewModel.incrementPercent()
                repeatHandler.postDelayed(RptUpdater(), delay)
                time += delay
            } else if (mAutoDecrement) {
                viewModel.decrementPercent()
                repeatHandler.postDelayed(RptUpdater(), delay)
                time += delay
            }
        }
    }

    private fun tryStartScan() {

        when (PermissionChecker.checkSelfPermission(
            activity as Context,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )) {
            PackageManager.PERMISSION_GRANTED -> {
                val appContext = activity?.applicationContext
                if (appContext != null)
                    viewModel.startScan(appContext)
//                Toast.makeText(activity, "Hello !", Toast.LENGTH_LONG).show()
            }

            else ->
                requestPermissions(arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), 1)
        }


    }

    /**
     * Enum for all buttons of UI
     */
    private enum class ButtonsTypes { LIGHT, BLIND, RADIATOR }
}

