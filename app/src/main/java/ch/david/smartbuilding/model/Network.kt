package ch.david.smartbuilding.model

import com.google.gson.annotations.SerializedName

/**
 * @author David Wittwer & Joao Mendes
 *
 * @version 0.1
 *
 * Created on 25.10.18 by David Wittwer
 */
class Network {
    @SerializedName("Network Home ID")
    var networkHomeID: String = ""
}