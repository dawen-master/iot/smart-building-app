package ch.david.smartbuilding.model

/**
 * @author David Wittwer & Joao Mendes
 *
 * @version 0.1
 *
 * Created on 28/10/18 by David Wittwer
 */
class DimmerLevel(val node_id: String, val value: String)