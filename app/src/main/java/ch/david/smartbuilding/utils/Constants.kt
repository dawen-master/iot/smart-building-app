package ch.david.smartbuilding.utils

/**
 * @author David Wittwer & Joao Mendes
 *
 * @version 0.1
 *
 * Created on 25.10.18 by David Wittwer
 */

const val BASE_URL = "http://192.168.1.2:5000"
const val KNX_BASE_URL = "http://192.168.1.137:5000"