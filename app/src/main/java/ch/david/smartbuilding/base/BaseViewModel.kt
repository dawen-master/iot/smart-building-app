package ch.david.smartbuilding.base

import android.arch.lifecycle.ViewModel
import ch.david.smartbuilding.injection.component.DaggerViewModelInjector
import ch.david.smartbuilding.injection.component.ViewModelInjector
import ch.david.smartbuilding.injection.module.NetworkModule
import ch.david.smartbuilding.ui.main.MainViewModel

/**
 * @author David Wittwer & Joao Mendes
 *
 * @version 0.1
 *
 * Created on 26/10/18 by David Wittwer
 */
abstract class BaseViewModel : ViewModel() {
    private val injector: ViewModelInjector = DaggerViewModelInjector
        .builder()
        .networkModule(NetworkModule)
        .build()

    init {
        inject()
    }

    private fun inject() {
        when (this) {
            is MainViewModel -> injector.inject(this)
        }
    }
}